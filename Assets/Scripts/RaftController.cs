using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaftController : MonoBehaviour
{
    public GameObject wood;
    public float raftSpeed = 100.0f;
    private float woodWidth = 0.4f;
    private bool isLevelComplited = false;
    void Start()
    {
        woodWidth = wood.transform.Find("WoodPart").gameObject.transform.localScale.z;
    }

    void Update()
    {
        //move raft if level completed
        if (isLevelComplited)
        {
            Vector3 direction = Vector3.forward * raftSpeed * Time.deltaTime;
            transform.position += direction;
        }
    }

    public void AddWoodToRaft(int currentWoodCount)
    {
        //place the wood next to previous one
        Vector3 woodPos = transform.position + new Vector3(0, 0, currentWoodCount * woodWidth);
        GameObject newWood = Instantiate(wood, woodPos, transform.rotation);
        newWood.GetComponent<WoodController>().SetInRaft(true);
        newWood.transform.parent = transform;
    }
    void OnCollisionEnter(Collision collision)
    {
        //if player came to finish
        Canvas canvas = GameController.Instance.transform.Find("Canvas").gameObject.GetComponent<Canvas>();
        Text info = canvas.transform.Find("Info").gameObject.GetComponent<Text>();

        //the level completed?
        if (GameController.Instance.IsWoodCollected())
        {
            isLevelComplited = true;

            //turn off controls
            GameObject player = collision.gameObject;
            player.transform.parent = transform;

            PlayerController controller = player.GetComponent<PlayerController>();
            controller.SetLevelCompleted();

            canvas.transform.Find("FixedJoystick").gameObject.SetActive(false);

            //congrats player
            info.text = "Level complited!";
            info.color = new Color(0, 255, 0);
        }

        //show info for 2 seconsd
        info.gameObject.SetActive(true);
        StartCoroutine(HideMessage(info.gameObject));
    }
    IEnumerator HideMessage(GameObject go)
    {
        yield return new WaitForSeconds(2);
        go.SetActive(false);
    }
}