using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodController : MonoBehaviour
{
    private bool isInRaft = false;
    public void SetInRaft(bool inRaft)
    {
        //turn off animation if the wood is in raft
        isInRaft = inRaft;
        gameObject.GetComponent<Animator>().SetBool("isInRaft", inRaft);
    }
    void OnCollisionEnter(Collision collision)
    {
        if (isInRaft)
            return;

        //collect wood if it's not in the raft
        GameController.Instance.AddWoodToRaft();
        Destroy(gameObject);
    }
}
