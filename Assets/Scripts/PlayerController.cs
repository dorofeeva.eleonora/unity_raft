﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public FixedJoystick joystick;
    public float speed = 5.0f;
    public Transform camTransform;
    public float camDistance = 7.0f;
    public bool isLevelCompleted = false;

    private Animator anim;
    private Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }
    public void Update()
    {
        if (!joystick || isLevelCompleted)
            return;

        //if running
        if (!Mathf.Approximately(joystick.Vertical, 0) || !Mathf.Approximately(joystick.Horizontal, 0))
        {
            transform.eulerAngles = new Vector3(0, 90 - Mathf.Atan2(joystick.Vertical, joystick.Horizontal) * 180 / Mathf.PI, 0);          
            Vector3 direction = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;
            rb.MovePosition(transform.position + direction * speed * Time.deltaTime);
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
    }
    private void LateUpdate()
    {
        //follow camera to player
        Vector3 dir = transform.position + new Vector3(0, camDistance, -camDistance);
        camTransform.position = dir;
    }

    public void SetLevelCompleted()
    {
        isLevelCompleted = true;
        anim.SetBool("isRunning", false);
    }
}
