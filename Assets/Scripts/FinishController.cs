using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishController : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        //load next level
        SceneManager.LoadScene("Level_01");
    }
}
