using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    public int startWoodCount = 5;
    public GameObject player, wood, raft, menu;
    public Transform field;

    private int currentWoodCount = 0;
    private Transform startPoint;
    private bool isGamePaused = true;

    static private int level = 0;
    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        //get raft and start point
        startPoint = field.Find("StartPoint").gameObject.transform;
        raft = field.Find("RaftPoint").gameObject;

        //spawn player
        GameObject curPlayer = Instantiate(player, startPoint.position, startPoint.rotation);
        PlayerController playerController = curPlayer.GetComponent<PlayerController>();
        playerController.camTransform = transform;
        playerController.joystick = transform.Find("Canvas").Find("FixedJoystick").gameObject.GetComponent<FixedJoystick>();
       
        //spawn wood
        /*����� �������� � ��������� �����, ����� �� ��������, � ����������� �� �����
         * */
        for (int i = 0; i < startWoodCount; ++i)
        {
            Vector3 woodPos = field.position + new Vector3(0, 1, i * 2);
            GameObject newWood = Instantiate(wood, woodPos, Quaternion.Euler(0, 0, 20));
            newWood.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }

        UpdateLevel();
        UpdateScore();
    }
    void UpdateScore()
    {
        Canvas canvas = transform.Find("Canvas").GetComponent<Canvas>();
        Text woodText = canvas.transform.Find("Wood").GetComponent<Text>();
        woodText.text = string.Format("Woods: {0}/{1}", currentWoodCount, startWoodCount);
    }
    void UpdateLevel()
    {
        /*��������������� ���� � ��� �� �������, �������� ������ �����
         */
        level++;
        Canvas canvas = transform.Find("Canvas").GetComponent<Canvas>();
        Text levelText = canvas.transform.Find("Level").GetComponent<Text>();
        levelText.text = string.Format("Level: {0}", level);

        if (level > 1)
        {
            /*���� ���������� ������ �� ������ ������ - 
             * ��� �������� ����� �������� ���� �� ����������
             */
            OnPlay();
        }
    }
    public void AddWoodToRaft()
    {
        raft.GetComponent<RaftController>().AddWoodToRaft(currentWoodCount);
        currentWoodCount++;
        UpdateScore();
    }
    public bool IsWoodCollected()
    {
        return currentWoodCount == startWoodCount;
    }
    
    public void OnPause()
    {
        isGamePaused = true;
        //show menu
        Canvas canvas = transform.Find("Canvas").GetComponent<Canvas>();
        GameObject panel = canvas.transform.Find("Panel").gameObject;
        panel.SetActive(isGamePaused);
        Time.timeScale = 0f;
    }
    public void OnPlay()
    {
        isGamePaused = false;
        //hide menu
        Canvas canvas = transform.Find("Canvas").GetComponent<Canvas>();
        GameObject panel = canvas.transform.Find("Panel").gameObject;
        panel.SetActive(isGamePaused);
        Time.timeScale = 1f;
    }
    public void OnQuit()
    {
        Application.Quit();
    }
}
